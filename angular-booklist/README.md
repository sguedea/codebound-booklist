# AngularBooklist

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

__Today we will be working on updating Codebound Bestseller BookList website!__

# INSTRUCTIONS
1. Fork this repository
2. Clone the forked repository.
3. Read through the existing code that is already on here.

# TODO
For this project, we will be using axios from NPM. 
1. Go to https://developer.nytimes.com/get-started
2. READ through the instructions to receive an API Key
3. We will be using the Book API
4. Give your App an name 
5. There is an API Key in the code provided, REPLACE the provide API key with your or your parnter's API key 


Stories:
* As a user, I would like the cover of the book to be displayed along with the title and author, so that I can see which book is which
* As a user, I would like a different color scheme and over theme to the webpage.
* As a user, I would like a different overall layout of the application.
* As a user, I would like to have a link to the book's amazon url, so that I can purchase the book on Amazon (this is a property available in the array) 
* As a user, I would like a navbar with a logo, and the name of the webpage.
* As a user, I would like a link in the navbar that will send me to a webpage to Blockbuster's History.
* As a user, I would like to be a form available to me, so that I can enter a NEW book
* As a user, I would like for the new book that I entered to be listed at the top of the list 
* As a user, I would like to be able to UPDATE a book's information
* As a user, I would like to be able to DELETE a book for the displayed list.

Feature:
* As a user, I would like to have the description of the book (this is a property available in the array)
* As a user, when I click on the book cover, I would see the description of that book appear on the application