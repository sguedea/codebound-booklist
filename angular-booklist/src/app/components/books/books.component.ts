import { Component, OnInit } from '@angular/core';
import {Book} from "../../models/Book";
import {BooksService} from "../../services/books.service";

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: Book[];
  currentBook: Book = {
    id: 0,
    isbn: 0,
    title: '',
    author: '',
    // book_image: '',
    // amazon_product_url: '',
    description: ''
  };

  isEdit: boolean = false;
  constructor(private booksService: BooksService) { }

  ngOnInit(): void {

    this.booksService.getBooks().subscribe(b => {
      console.log(b.results.books);
      this.books = b.results.books;

      // console.log(b.results.books);
      // this.books = b.results.books;
    })
  }

  onNewBook(book: Book) {
    this.books.unshift(book);
  }

  editBook(book: Book) {
    this.currentBook = book;
    // included for the update post button
    this.isEdit = true;
  }

  onUpdateBook(book: Book) {
    this.books.forEach((cur, index) => {
      if (book.id === cur.id) {

        // splicing the old post
        this.books.splice(index, 1);

        // replacing the old post with the updated one,
        // AND putting the updated post to the top of the list.
        this.books.unshift(book);

        // resetting our button to 'Submit Post' btn
        this.isEdit = false;

        // resetting our form
        this.currentBook = {
          id: 0,
          isbn: 0,
          title: '',
          author: '',
          // book_image: '',
          // amazon_product_url: '',
          description: ''
        };

      }
    })
  }

  // create a method that will delete a single post
  deleteBook(book: Book) {
    if (confirm('Are you sure?')) {
      this.booksService.removeBook(book.isbn).subscribe(() => {
        this.books.forEach((current, index) => {
          if (book.isbn === current.isbn) {
            this.books.slice(index, 1);
          }
        })
      })
    }
  }

}
