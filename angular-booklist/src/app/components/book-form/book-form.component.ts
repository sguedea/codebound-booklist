import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {Book} from "../../models/Book";
import {BooksService} from "../../services/books.service";

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  // PROPERTIES
  showBookForm: boolean = false;
  enableAddBook: boolean;



  @Output() newBook: EventEmitter<Book> = new EventEmitter();
  @Output() updatedBook: EventEmitter<Book> = new EventEmitter();

  @Input() currentBook: Book;
  @Input() isEdit: boolean;

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {
    this.enableAddBook = true;

  }

  addBook(title, author, description) {
    if (!title || !author) {
      alert('Please enter a book')
    } else {
      this.booksService.saveBook({title, author, description} as Book).subscribe(book => {
        this.newBook.emit(book);
      })
    }
  }

  updateBook() {
    // console.log("Updating post... ");
    this.booksService.updateBook(this.currentBook).subscribe(book => {
      console.log(book);
      this.isEdit = false;
      this.updatedBook.emit(book)
    })
  }

}
