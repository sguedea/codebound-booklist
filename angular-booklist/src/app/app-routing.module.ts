import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BooksComponent } from "./components/books/books.component";
import {HomeComponent} from "./components/home/home.component";


const routes: Routes = [
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'books', component: BooksComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
