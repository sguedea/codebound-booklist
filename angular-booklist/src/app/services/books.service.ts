import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable } from "rxjs";
import {Book} from "../models/Book";

const httpOptions  = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  apiKey: string = '?api-key=wlPeYsWGJ6pGHndGGx9RG1XGEqnwJjBZ';
  booksURL: string = 'https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json' + this.apiKey;



  constructor(private http: HttpClient) { }

  getBooks() : Observable<Book[]> {

    return this.http.get<Book[]>(this.booksURL);
  }

  saveBook(book : Book) : Observable<Book> {
    return this.http.post<Book>(this.booksURL, book, httpOptions)
  }

  updateBook(book : Book) : Observable<Book> {
    const url = `${this.booksURL}/${book.id}`;

    return this.http.put<Book>(url, book, httpOptions)
  }

  removeBook(book : Book ) : Observable<Book> {
    const id = typeof book === 'number' ? book : book.isbn;

    const url = `${this.booksURL}/${id}`;

    console.log('Deleting post...');
    alert('Post removed');


    return this.http.delete<Book>(url, httpOptions);
  }
}
