export interface Book {
  // id: number,
  // title: string,
  // author: string,
  // img?: number,
  id?: number,
  isbn?: number,
  title: string,
  author: string,
  book_image?: string,
  amazon_product_url?: string,
  description?: string

}
